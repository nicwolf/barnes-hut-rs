use std::ops::Add;
use std::ops::AddAssign;

use crate::vec2::Vec2;

#[derive(Debug, PartialEq)]
pub struct Body {
    pub mass: f32,
    pub position: Vec2,
}

impl Body {
    pub fn distance_to(&self, other: &Body) -> f32 {
        let x = self.position.x - other.position.x;
        let y = self.position.y - other.position.y;
        (x.powf(2.0) + y.powf(2.0)).sqrt()
    }
}

impl Add for Body {
    type Output = Self;
    fn add(self, other: Body) -> Body {
        let mass = self.mass + other.mass;
        Self {
            mass,
            position: Vec2 {
                x: (self.position.x * self.mass + other.position.x * other.mass) / mass,
                y: (self.position.y * self.mass + other.position.y * other.mass) / mass,
            },
        }
    }
}

impl<'a> Add<Body> for &'a Body {
    type Output = Body;
    fn add(self, other: Body) -> Body {
        let mass = self.mass + other.mass;
        Body {
            mass,
            position: Vec2 {
                x: (self.position.x * self.mass + other.position.x * other.mass) / mass,
                y: (self.position.y * self.mass + other.position.y * other.mass) / mass,
            },
        }
    }
}

impl<'a> Add<&'a Body> for Body {
    type Output = Body;
    fn add(self, other: &'a Body) -> Body {
        let mass = self.mass + other.mass;
        Body {
            mass,
            position: Vec2 {
                x: (self.position.x * self.mass + other.position.x * other.mass) / mass,
                y: (self.position.y * self.mass + other.position.y * other.mass) / mass,
            },
        }
    }
}

impl<'a, 'b> Add<&'b Body> for &'a Body {
    type Output = Body;
    fn add(self, other: &'b Body) -> Body {
        let mass = self.mass + other.mass;
        Body {
            mass,
            position: Vec2 {
                x: (self.position.x * self.mass + other.position.x * other.mass) / mass,
                y: (self.position.y * self.mass + other.position.y * other.mass) / mass,
            },
        }
    }
}

impl AddAssign for Body {
    fn add_assign(&mut self, other: Self) {
        let mass = self.mass + other.mass;
        *self = Self {
            mass,
            position: Vec2 {
                x: (self.position.x * self.mass + other.position.x * other.mass) / mass,
                y: (self.position.y * self.mass + other.position.y * other.mass) / mass,
            },
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Body;
    use super::Vec2;

    #[test]
    fn distance_to() {
        let b1 = Body {
            position: Vec2 { x: 0.0, y: 0.0 },
            mass: 0.0,
        };
        let b2 = Body {
            position: Vec2 { x: 1.0, y: 1.0 },
            mass: 0.0,
        };
        let dot_product: f32 = 2.0;
        assert_eq!(b1.distance_to(&b2), dot_product.sqrt());
    }

    #[test]
    fn add_check_variants() {
        assert_eq!(
            Body {
                position: Vec2 { x: 0.0, y: 0.0 },
                mass: 1.0,
            } + Body {
                position: Vec2 { x: 1.0, y: 1.0 },
                mass: 1.0,
            },
            Body {
                position: Vec2 { x: 0.5, y: 0.5 },
                mass: 2.0,
            }
        );
        assert_eq!(
            &Body {
                position: Vec2 { x: 0.0, y: 0.0 },
                mass: 1.0,
            } + Body {
                position: Vec2 { x: 1.0, y: 1.0 },
                mass: 1.0,
            },
            Body {
                position: Vec2 { x: 0.5, y: 0.5 },
                mass: 2.0,
            }
        );
        assert_eq!(
            Body {
                position: Vec2 { x: 0.0, y: 0.0 },
                mass: 1.0,
            } + &Body {
                position: Vec2 { x: 1.0, y: 1.0 },
                mass: 1.0,
            },
            Body {
                position: Vec2 { x: 0.5, y: 0.5 },
                mass: 2.0,
            }
        );
        assert_eq!(
            &Body {
                position: Vec2 { x: 0.0, y: 0.0 },
                mass: 1.0,
            } + &Body {
                position: Vec2 { x: 1.0, y: 1.0 },
                mass: 1.0,
            },
            Body {
                position: Vec2 { x: 0.5, y: 0.5 },
                mass: 2.0,
            }
        );
    }

    #[test]
    fn add_check_center_of_mass() {
        assert_eq!(
            Body {
                position: Vec2 { x: 0.0, y: 0.0 },
                mass: 1.0,
            } + Body {
                position: Vec2 { x: 1.0, y: 1.0 },
                mass: 2.0,
            },
            Body {
                position: Vec2 {
                    x: 2.0 / 3.0,
                    y: 2.0 / 3.0
                },
                mass: 3.0,
            }
        );
    }
}
