pub struct Simulation {
  quadrant: [Vec2; 2],
}

impl Simulation {
  pub fn new() -> Simulation {
    Simulation {
      quadrant: Quadrant {
        lower_left: Vec2 { x: 0.0, y: 0.0 },
        upper_right: Vec2 { x: 1.0, y: 1.0 },
      },
    }
  }
}

#[cfg(test)]
mod tests {
  use super::Simulation;
  #[test]
  fn tick() {}
}
