use crate::vec2::Vec2;

#[derive(Debug, PartialEq)]
pub struct Quadrant {
    pub lower_left: Vec2,
    pub upper_right: Vec2,
}

impl Quadrant {
    pub fn length(&self) -> f32 {
        self.upper_right.x - self.lower_left.x
    }
    pub fn contains(&self, p: &Vec2) -> bool {
        p.x >= self.lower_left.x
            && p.x <= self.upper_right.x
            && p.y >= self.lower_left.y
            && p.y <= self.upper_right.y
    }
    pub fn nw(&self) -> Quadrant {
        Quadrant {
            lower_left: Vec2 {
                x: self.lower_left.x,
                y: (self.lower_left.y + self.upper_right.y) / 2.0,
            },
            upper_right: Vec2 {
                x: (self.lower_left.x + self.upper_right.y) / 2.0,
                y: self.upper_right.y,
            },
        }
    }
    pub fn ne(&self) -> Quadrant {
        Quadrant {
            lower_left: Vec2 {
                x: (self.lower_left.x + self.upper_right.y) / 2.0,
                y: (self.lower_left.y + self.upper_right.y) / 2.0,
            },
            upper_right: Vec2 {
                x: self.upper_right.x,
                y: self.upper_right.y,
            },
        }
    }
    pub fn sw(&self) -> Quadrant {
        Quadrant {
            lower_left: Vec2 {
                x: self.lower_left.x,
                y: self.lower_left.y,
            },
            upper_right: Vec2 {
                x: (self.lower_left.x + self.upper_right.y) / 2.0,
                y: (self.lower_left.y + self.upper_right.y) / 2.0,
            },
        }
    }
    pub fn se(&self) -> Quadrant {
        Quadrant {
            lower_left: Vec2 {
                x: (self.lower_left.x + self.upper_right.y) / 2.0,
                y: self.lower_left.y,
            },
            upper_right: Vec2 {
                x: self.upper_right.x,
                y: (self.lower_left.y + self.upper_right.y) / 2.0,
            },
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Quadrant;
    use super::Vec2;
    #[test]
    fn length() {
        assert_eq!(
            Quadrant {
                lower_left: Vec2 { x: 0.0, y: 0.0 },
                upper_right: Vec2 { x: 10.0, y: 10.0 },
            }
            .length(),
            10.0
        );
    }

    #[test]
    fn contains() {
        let quadrant = Quadrant {
            lower_left: Vec2 { x: 0.0, y: 0.0 },
            upper_right: Vec2 { x: 1.0, y: 1.0 },
        };
        assert!(quadrant.contains(&Vec2 { x: 0.5, y: 0.5 }));
        assert!(quadrant.contains(&Vec2 { x: 0.0, y: 0.0 }));
        assert!(quadrant.contains(&Vec2 { x: 1.0, y: 1.0 }));
        assert!(!quadrant.contains(&Vec2 { x: 1.5, y: 1.5 }));
        assert!(!quadrant.contains(&Vec2 { x: 1.5, y: -1.5 }));
    }

    #[test]
    fn subdivisions() {
        let quadrant = Quadrant {
            lower_left: Vec2 { x: 0.0, y: 0.0 },
            upper_right: Vec2 { x: 1.0, y: 1.0 },
        };
        let sw = Quadrant {
            lower_left: Vec2 { x: 0.0, y: 0.0 },
            upper_right: Vec2 { x: 0.5, y: 0.5 },
        };
        let se = Quadrant {
            lower_left: Vec2 { x: 0.5, y: 0.0 },
            upper_right: Vec2 { x: 1.0, y: 0.5 },
        };
        let nw = Quadrant {
            lower_left: Vec2 { x: 0.0, y: 0.5 },
            upper_right: Vec2 { x: 0.5, y: 1.0 },
        };
        let ne = Quadrant {
            lower_left: Vec2 { x: 0.5, y: 0.5 },
            upper_right: Vec2 { x: 1.0, y: 1.0 },
        };
        assert_eq!(quadrant.sw(), sw);
        assert_eq!(quadrant.se(), se);
        assert_eq!(quadrant.nw(), nw);
        assert_eq!(quadrant.ne(), ne);
    }
}
