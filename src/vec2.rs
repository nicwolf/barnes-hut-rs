use std::ops::{Add, Sub};

#[derive(Debug, PartialEq)]
pub struct Vec2 {
    pub x: f32,
    pub y: f32,
}

impl Vec2 {
    pub fn magnitude(&self) -> f32 {
        (self.x.powf(2.0) + self.y.powf(2.0)).sqrt()
    }
}

impl Add for Vec2 {
    type Output = Self;
    fn add(self, other: Vec2) -> Vec2 {
        Vec2 {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl<'a> Add<Vec2> for &'a Vec2 {
    type Output = Vec2;
    fn add(self, other: Vec2) -> Vec2 {
        Vec2 {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl<'a> Add<&'a Vec2> for Vec2 {
    type Output = Vec2;
    fn add(self, other: &'a Vec2) -> Vec2 {
        Vec2 {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl<'a, 'b> Add<&'b Vec2> for &'a Vec2 {
    type Output = Vec2;
    fn add(self, other: &'b Vec2) -> Vec2 {
        Vec2 {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl Sub for Vec2 {
    type Output = Self;
    fn sub(self, other: Vec2) -> Vec2 {
        Vec2 {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl<'a> Sub<Vec2> for &'a Vec2 {
    type Output = Vec2;
    fn sub(self, other: Vec2) -> Vec2 {
        Vec2 {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl<'a> Sub<&'a Vec2> for Vec2 {
    type Output = Vec2;
    fn sub(self, other: &'a Vec2) -> Vec2 {
        Vec2 {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl<'a, 'b> Sub<&'b Vec2> for &'a Vec2 {
    type Output = Vec2;
    fn sub(self, other: &'b Vec2) -> Vec2 {
        Vec2 {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Vec2;
    #[test]
    fn magnitude() {
        let vec: Vec2 = Vec2 { x: 1.0, y: 0.0 };
        assert_eq!(vec.magnitude(), 1.0);
        let vec: Vec2 = Vec2 { x: 3.0, y: 4.0 };
        assert_eq!(vec.magnitude(), 5.0);
        let vec: Vec2 = Vec2 { x: 1.0, y: -1.0 };
        let dot_product: f32 = 2.0;
        assert_eq!(vec.magnitude(), dot_product.sqrt());
    }
    #[test]
    fn add() {
        assert_eq!(
            Vec2 { x: 0.0, y: 1.0 } + Vec2 { x: 1.0, y: 0.0 },
            Vec2 { x: 1.0, y: 1.0 }
        );
        assert_eq!(
            &Vec2 { x: 0.0, y: 1.0 } + Vec2 { x: 1.0, y: 0.0 },
            Vec2 { x: 1.0, y: 1.0 }
        );
        assert_eq!(
            Vec2 { x: 0.0, y: 1.0 } + &Vec2 { x: 1.0, y: 0.0 },
            Vec2 { x: 1.0, y: 1.0 }
        );
        assert_eq!(
            &Vec2 { x: 0.0, y: 1.0 } + &Vec2 { x: 1.0, y: 0.0 },
            Vec2 { x: 1.0, y: 1.0 }
        );
    }
    #[test]
    fn sub() {
        assert_eq!(
            Vec2 { x: 0.0, y: 1.0 } - Vec2 { x: 1.0, y: 0.0 },
            Vec2 { x: -1.0, y: 1.0 }
        );
        assert_eq!(
            &Vec2 { x: 0.0, y: 1.0 } - Vec2 { x: 1.0, y: 0.0 },
            Vec2 { x: -1.0, y: 1.0 }
        );
        assert_eq!(
            Vec2 { x: 0.0, y: 1.0 } - &Vec2 { x: 1.0, y: 0.0 },
            Vec2 { x: -1.0, y: 1.0 }
        );
        assert_eq!(
            &Vec2 { x: 0.0, y: 1.0 } - &Vec2 { x: 1.0, y: 0.0 },
            Vec2 { x: -1.0, y: 1.0 }
        );
    }
}
