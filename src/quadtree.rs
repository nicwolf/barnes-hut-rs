use crate::body::Body;
use crate::quadrant::Quadrant;
use crate::vec2::Vec2;

struct QuadtreeIterator<'a> {
  stack: Vec<&'a Quadtree>,
}

impl<'a> Iterator for QuadtreeIterator<'a> {
  type Item = &'a Quadtree;
  fn next(&mut self) -> Option<&'a Quadtree> {
    if self.stack.len() == 0 {
      None
    } else {
      let cur: Option<&'a Quadtree> = self.stack.pop();
      for quadtree in QuadtreeIterator { stack: vec!(&cur)} {
        if let Some(tree) = &quadtree.nw { for t in tree.nw.iter() { self.stack.push(&**t) } }
        else if let Some(tree) = &quadtree.ne { for t in tree.ne.iter() { self.stack.push(&**t) } }
        else if let Some(tree) = &quadtree.se { for t in tree.se.iter() { self.stack.push(&**t) } }
        else if let Some(tree) = &quadtree.sw { for t in tree.sw.iter() { self.stack.push(&**t) } }
      }
      cur
    }
  }
}

#[derive(Debug, PartialEq)]
enum QuadtreeType {
  EXTERNAL,
  INTERNAL,
}

#[derive(Debug, PartialEq)]
enum QuadtreeChildren {
  NW,
  NE,
  SE,
  SW,
}

#[derive(Debug, PartialEq)]
pub struct Quadtree {
  bounding_box: Quadrant,
  body: Option<Body>,
  r#type: QuadtreeType,
  nw: Option<Box<Quadtree>>,
  ne: Option<Box<Quadtree>>,
  se: Option<Box<Quadtree>>,
  sw: Option<Box<Quadtree>>,
}

impl Quadtree {
  pub fn new(bounding_box: Quadrant) -> Quadtree {
    Quadtree {
      bounding_box,
      body: None,
      r#type: QuadtreeType::EXTERNAL,
      nw: None,
      ne: None,
      se: None,
      sw: None,
    }
  }
  fn subdivide(&mut self) {
    self.r#type = QuadtreeType::INTERNAL;
    self.nw = Some(Box::new(Quadtree::new(self.bounding_box.nw())));
    self.ne = Some(Box::new(Quadtree::new(self.bounding_box.ne())));
    self.se = Some(Box::new(Quadtree::new(self.bounding_box.se())));
    self.sw = Some(Box::new(Quadtree::new(self.bounding_box.sw())));
  }
  fn locate_body_in_children(&self, body: &Body) -> QuadtreeChildren {
    if self
      .nw
      .as_ref()
      .unwrap()
      .bounding_box
      .contains(&body.position)
    {
      return QuadtreeChildren::NW;
    } else if self
      .ne
      .as_ref()
      .unwrap()
      .bounding_box
      .contains(&body.position)
    {
      return QuadtreeChildren::NE;
    } else if self
      .se
      .as_ref()
      .unwrap()
      .bounding_box
      .contains(&body.position)
    {
      return QuadtreeChildren::SE;
    }
    return QuadtreeChildren::SW;
  }
  pub fn insert(&mut self, body: Body) {
    // If the root of this quadtree does not contain a body, put the new
    // body there.
    match self.body {
      None => self.body = Some(body),
      Some(_) => {
        match self.r#type {
          QuadtreeType::EXTERNAL => {
            // If the root of this quadtree is an external node, we now have two
            // bodies in the same region. Subdivide the region into four quadrants
            // and recursively insert the root's body and the new body into the
            // appropriate quadrant(s).
            self.subdivide();
            let old_body = self.body.take().unwrap();
            self.body = Some(&old_body + &body);
            match self.locate_body_in_children(&old_body) {
              QuadtreeChildren::NW => self.nw.as_mut().unwrap().insert(old_body),
              QuadtreeChildren::NE => self.ne.as_mut().unwrap().insert(old_body),
              QuadtreeChildren::SE => self.se.as_mut().unwrap().insert(old_body),
              QuadtreeChildren::SW => self.sw.as_mut().unwrap().insert(old_body),
            }
            match self.locate_body_in_children(&body) {
              QuadtreeChildren::NW => self.nw.as_mut().unwrap().insert(body),
              QuadtreeChildren::NE => self.ne.as_mut().unwrap().insert(body),
              QuadtreeChildren::SE => self.se.as_mut().unwrap().insert(body),
              QuadtreeChildren::SW => self.sw.as_mut().unwrap().insert(body),
            }
          },
          QuadtreeType::INTERNAL => {
            // If the root of this quadtree is an internal node, update the root's
            // body and insert the new body into the appropriate child.
            let old_body = self.body.take().unwrap();
            self.body = Some(&old_body + &body);
            match self.locate_body_in_children(&body) {
              QuadtreeChildren::NW => self.nw.as_mut().unwrap().insert(body),
              QuadtreeChildren::NE => self.ne.as_mut().unwrap().insert(body),
              QuadtreeChildren::SE => self.se.as_mut().unwrap().insert(body),
              QuadtreeChildren::SW => self.sw.as_mut().unwrap().insert(body),
            }
          },
        }
      }
    }
  }
}

#[cfg(test)]
mod tests {
  use super::Body;
  use super::Quadrant;
  use super::Quadtree;
  use super::QuadtreeIterator;
  use super::QuadtreeType;
  use super::Vec2;

  fn populate_quadtree() -> Quadtree {
    let nw = Quadtree {
      bounding_box: Quadrant {
        lower_left: Vec2 { x: 0.0, y: 0.5 },
        upper_right: Vec2 { x: 0.5, y: 1.0 },
      },
      body: None,
      r#type: QuadtreeType::EXTERNAL,
      nw: None,
      ne: None,
      se: None,
      sw: None,
    };

    let ne = Quadtree {
      bounding_box: Quadrant {
        lower_left: Vec2 { x: 0.5, y: 0.5 },
        upper_right: Vec2 { x: 1.0, y: 1.0 },
      },
      body: Some(Body {
        position: Vec2 { x: 0.75, y: 0.75 },
        mass: 1.0,
      }),
      r#type: QuadtreeType::EXTERNAL,
      nw: None,
      ne: None,
      se: None,
      sw: None,
    };

    let se = Quadtree {
      bounding_box: Quadrant {
        lower_left: Vec2 { x: 0.5, y: 0.0 },
        upper_right: Vec2 { x: 1.0, y: 0.5 },
      },
      body: None,
      r#type: QuadtreeType::EXTERNAL,
      nw: None,
      ne: None,
      se: None,
      sw: None,
    };

    let sw = Quadtree {
      bounding_box: Quadrant {
        lower_left: Vec2 { x: 0.0, y: 0.0 },
        upper_right: Vec2 { x: 0.5, y: 0.5 },
      },
      body: Some(Body {
        position: Vec2 { x: 0.25, y: 0.25 },
        mass: 1.0,
      }),
      r#type: QuadtreeType::EXTERNAL,
      nw: None,
      ne: None,
      se: None,
      sw: None,
    };

    Quadtree {
      bounding_box: Quadrant {
        lower_left: Vec2 { x: 0.0, y: 0.0 },
        upper_right: Vec2 { x: 1.0, y: 1.0 },
      },
      body: Some(Body {
        position: Vec2 { x: 0.5, y: 0.5 },
        mass: 2.0,
      }),
      r#type: QuadtreeType::INTERNAL,
      nw: Some(Box::new(nw)),
      ne: Some(Box::new(ne)),
      se: Some(Box::new(se)),
      sw: Some(Box::new(sw)),
    }
  }

  #[test]
  fn insert_into_empty_external() {
    let mut quadtree = Quadtree::new(Quadrant {
      lower_left: Vec2 { x: 0.0, y: 0.0 },
      upper_right: Vec2 { x: 1.0, y: 1.0 },
    });
    let body = Body {
      position: Vec2 { x: 0.25, y: 0.25 },
      mass: 1.0,
    };
    quadtree.insert(body);
    assert_eq!(
      quadtree,
      Quadtree {
        bounding_box: Quadrant {
          lower_left: Vec2 { x: 0.0, y: 0.0 },
          upper_right: Vec2 { x: 1.0, y: 1.0 },
        },
        body: Some(Body {
          position: Vec2 { x: 0.25, y: 0.25 },
          mass: 1.0,
        }),
        r#type: QuadtreeType::EXTERNAL,
        nw: None,
        ne: None,
        se: None,
        sw: None,
      }
    )
  }

  #[test]
  fn insert_into_internal() {
    let mut quadtree = Quadtree::new(Quadrant {
      lower_left: Vec2 { x: 0.0, y: 0.0 },
      upper_right: Vec2 { x: 1.0, y: 1.0 },
    });
    let body1 = Body {
      position: Vec2 { x: 0.25, y: 0.25 },
      mass: 1.0,
    };
    let body2 = Body {
      position: Vec2 { x: 0.75, y: 0.75 },
      mass: 1.0,
    };
    let body3 = Body {
      position: Vec2 { x: 0.50, y: 0.50 },
      mass: 1.0,
    };
    quadtree.insert(body1);
    quadtree.insert(body2);
    quadtree.insert(body3);

    let nw = Quadtree {
      bounding_box: Quadrant {
        lower_left: Vec2 { x: 0.0, y: 0.5 },
        upper_right: Vec2 { x: 0.5, y: 1.0 },
      },
      body: Some(Body {
        position: Vec2 { x: 0.50, y: 0.50 },
        mass: 1.0,
      }),
      r#type: QuadtreeType::EXTERNAL,
      nw: None,
      ne: None,
      se: None,
      sw: None,
    };

    let ne = Quadtree {
      bounding_box: Quadrant {
        lower_left: Vec2 { x: 0.5, y: 0.5 },
        upper_right: Vec2 { x: 1.0, y: 1.0 },
      },
      body: Some(Body {
        position: Vec2 { x: 0.75, y: 0.75 },
        mass: 1.0,
      }),
      r#type: QuadtreeType::EXTERNAL,
      nw: None,
      ne: None,
      se: None,
      sw: None,
    };

    let se = Quadtree {
      bounding_box: Quadrant {
        lower_left: Vec2 { x: 0.5, y: 0.0 },
        upper_right: Vec2 { x: 1.0, y: 0.5 },
      },
      body: None,
      r#type: QuadtreeType::EXTERNAL,
      nw: None,
      ne: None,
      se: None,
      sw: None,
    };

    let sw = Quadtree {
      bounding_box: Quadrant {
        lower_left: Vec2 { x: 0.0, y: 0.0 },
        upper_right: Vec2 { x: 0.5, y: 0.5 },
      },
      body: Some(Body {
        position: Vec2 { x: 0.25, y: 0.25 },
        mass: 1.0,
      }),
      r#type: QuadtreeType::EXTERNAL,
      nw: None,
      ne: None,
      se: None,
      sw: None,
    };

    let root = Quadtree {
      bounding_box: Quadrant {
        lower_left: Vec2 { x: 0.0, y: 0.0 },
        upper_right: Vec2 { x: 1.0, y: 1.0 },
      },
      body: Some(Body {
        position: Vec2 { x: 0.5, y: 0.5 },
        mass: 3.0,
      }),
      r#type: QuadtreeType::INTERNAL,
      nw: Some(Box::new(nw)),
      ne: Some(Box::new(ne)),
      se: Some(Box::new(se)),
      sw: Some(Box::new(sw)),
    };

    assert_eq!(quadtree, root);
  }

  #[test]
  fn insert_into_populated_external() {
    let mut quadtree = Quadtree::new(Quadrant {
      lower_left: Vec2 { x: 0.0, y: 0.0 },
      upper_right: Vec2 { x: 1.0, y: 1.0 },
    });
    let body1 = Body {
      position: Vec2 { x: 0.25, y: 0.25 },
      mass: 1.0,
    };
    let body2 = Body {
      position: Vec2 { x: 0.75, y: 0.75 },
      mass: 1.0,
    };
    quadtree.insert(body1);
    quadtree.insert(body2);

    let nw = Quadtree {
      bounding_box: Quadrant {
        lower_left: Vec2 { x: 0.0, y: 0.5 },
        upper_right: Vec2 { x: 0.5, y: 1.0 },
      },
      body: None,
      r#type: QuadtreeType::EXTERNAL,
      nw: None,
      ne: None,
      se: None,
      sw: None,
    };

    let ne = Quadtree {
      bounding_box: Quadrant {
        lower_left: Vec2 { x: 0.5, y: 0.5 },
        upper_right: Vec2 { x: 1.0, y: 1.0 },
      },
      body: Some(Body {
        position: Vec2 { x: 0.75, y: 0.75 },
        mass: 1.0,
      }),
      r#type: QuadtreeType::EXTERNAL,
      nw: None,
      ne: None,
      se: None,
      sw: None,
    };

    let se = Quadtree {
      bounding_box: Quadrant {
        lower_left: Vec2 { x: 0.5, y: 0.0 },
        upper_right: Vec2 { x: 1.0, y: 0.5 },
      },
      body: None,
      r#type: QuadtreeType::EXTERNAL,
      nw: None,
      ne: None,
      se: None,
      sw: None,
    };

    let sw = Quadtree {
      bounding_box: Quadrant {
        lower_left: Vec2 { x: 0.0, y: 0.0 },
        upper_right: Vec2 { x: 0.5, y: 0.5 },
      },
      body: Some(Body {
        position: Vec2 { x: 0.25, y: 0.25 },
        mass: 1.0,
      }),
      r#type: QuadtreeType::EXTERNAL,
      nw: None,
      ne: None,
      se: None,
      sw: None,
    };

    let root = Quadtree {
      bounding_box: Quadrant {
        lower_left: Vec2 { x: 0.0, y: 0.0 },
        upper_right: Vec2 { x: 1.0, y: 1.0 },
      },
      body: Some(Body {
        position: Vec2 { x: 0.5, y: 0.5 },
        mass: 2.0,
      }),
      r#type: QuadtreeType::INTERNAL,
      nw: Some(Box::new(nw)),
      ne: Some(Box::new(ne)),
      se: Some(Box::new(se)),
      sw: Some(Box::new(sw)),
    };

    assert_eq!(quadtree, root);
  }

  #[test]
  fn iterate_over_children() {
    let quadtree = populate_quadtree();
    let mut iterator = QuadtreeIterator { stack: vec!(&quadtree) };
    let test = iterator.next();
    let test = iterator.next();
    assert_eq!(&quadtree, test.unwrap());
  }
}
